import React, { Component } from 'react';
import './App.css';
import 'bulma/css/bulma.css';
import 'font-awesome/css/font-awesome.css'

import Navbar from './component/Navbar'
import SectionName from './component/SectionName'
import BodyHeader from './component/BodyHeader'
import BodyInput from './component/BodyInput'

class App extends Component {
  render() {
    return (
      <div>
        <Navbar></Navbar>
        <SectionName></SectionName>
        <BodyHeader></BodyHeader>
        <BodyInput></BodyInput>
      </div>
    );
  }
}

export default App;
