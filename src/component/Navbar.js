import React, {Component} from 'react';

export default class Navbar extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return(
            <nav className="navbar">
                <div className="columns is-mobile level" style={{width: '100%'}}>
                    <div className="column is-half">
                        <div className="columns">
                            <div className="column is-one-third">
                                <figure className="image">
                                    <img src={require('../Assets/Images/RSU_dent_logo.png')} alt="aaa"/>
                                </figure>
                            </div>
                        </div>
                    </div>
                    <div className="column is-half level-right">
                        
                    </div>           
                </div>          
            </nav>
        )
	  
    }
}