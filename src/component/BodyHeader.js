import React, {Component} from 'react';

export default class BodyHeader extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return(
            <section className="section level" style={{paddingTop: '10px', paddingBottom: '5px', marginBottom: '15px'}}>
                <div className="columns is-mobile level-item has-text-centered body-input-header">
                    <div className="column is-3">
                        <span className="fa fa-hospital-o"> Department</span>
                    </div>
                    <div className="column is-3">
                        <span className="fa fa-user-md"> Instructor</span>
                    </div>
                    <div className="column is-3">
                        <span className="fa fa-user"> Fix Student</span>
                    </div>
                    <div className="column is-1">
                        <span className="fa fa-user"> FC</span>
                    </div>
                    <div className="column is-1">
                        <span className="fa fa-balance-scale"> Ev</span>
                    </div>
                </div>
            </section>
        )
	  
    }
}