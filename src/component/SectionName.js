import React, {Component} from 'react';

export default class SectionName extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return(
            <section className="hero hero-dent">
                <div className="hero-body level">
                    <div className="container level-item level-right">
                        <h1 className="title">
                            Dent RSU
                        </h1>
                    </div>
                </div>
            </section>            
        )
	  
    }
}