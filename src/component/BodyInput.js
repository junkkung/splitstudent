import React, {Component} from 'react';

export default class BodyInput extends Component {

    constructor(props) {
        super(props);

        this.state = {
            departments: [{
                departmentName: 'Oper',
                instructor: 0,
                student: 0,
                fc: 0,
                ev: 0
            },{
                departmentName: 'Perio',
                instructor: 0,
                student: 0,
                fc: 0,
                ev: 0
            },{
                departmentName: 'Endo',
                instructor: 0,
                student: 0,
                fc: 0,
                ev: 0
            },{
                departmentName: 'Prosth1',
                instructor: 0,
                student: 0,
                fc: 0,
                ev: 0
            },{
                departmentName: 'Prosth2',                
                instructor: 0,
                student: 0,
                fc: 0,
                ev: 0
            },{
                departmentName: 'Prosth3',                
                instructor: 0,
                student: 0,
                fc: 0,
                ev: 0
            },{
                departmentName: 'Prosth4',                
                instructor: 0,
                student: 0,
                fc: 0,
                ev: 0
            }],
            fcTotal: 0
        }
    }

    // componentWillMount() {
    //     console.log('wm');
    // }

    // componentDidMount() {
    //     console.log('dm');
    // }

    // componentDidUpdate(prevProps, prevState) {
    //     console.log('du');
    // }

    // componentWillUnmount() {
    //     console.log('wum');
    // }
    
    // componentWillUpdate(nextProps, nextState) {
    //     console.log('wu');
    // }    

    // componentWillReceiveProps(nextProps) {
    //     console.log('wrp');
    // }
    
    convertStateValue (object) {
        var stringify = JSON.stringify(object);
        return JSON.parse(stringify);
    }

    objectToArray (objects) {
        var array = [];
        for (var key in objects) {
            if (objects.hasOwnProperty(key)) {
                array.push(objects[key]);
            }
        }

        return array;
    }

    setInputValue(event) {
        var departmentName = event.currentTarget.getAttribute('department');
        var staff = event.currentTarget.getAttribute('staff');
        var value = parseInt(event.currentTarget.value, 10);
        var departments = this.convertStateValue(this.state.departments);

        departments.forEach((department)=>{
            if (department.departmentName === departmentName) {
                department[staff] = value;
            }
        });

        this.setState({
            departments: departments
        });
    }

    onCalculateClicked(event) {        
        var departments = this.convertStateValue(this.state.departments);
        departments = this.clearFcAndEvValue(departments);
        departments = this.everageStudentPerTeacher(departments);
        for (var i = 0; i < this.state.fcTotal; i++) {            
            var index = this.findLowestValue(departments);
            departments[index].fc += 1;
            departments = this.everageStudentPerTeacher(departments);   
        }

        this.setState({
            departments: departments
        });      
    }
      
    findLowestValue (departments) {
        var lowestEv = 9999999;
        var index;

        departments.forEach((department, i) => {
            if (department.ev !== null && department.ev < lowestEv) {
                lowestEv = department.ev;
                index = i;
            }
        });

        return index;
    }
    
    everageStudentPerTeacher (departments) {
        departments.forEach((department)=>{
            if (department.instructor && department.instructor !== 0) {
                department.ev = Math.round(((department.student + department.fc)/department.instructor) * 100) / 100;
            } else {
                department.ev = '-';
            }
        });

        return departments;
    }

    clearFcAndEvValue (departments) {
        departments.forEach((department)=>{
            if (department.fc) {
                department.fc = 0;
            }

            if (department.ev) {
                department.ev = 0;
            }
        });

        return departments;
    }

    render() {
        const rowForm = this.state.departments.map(department => (
            <section className="section level no-padding-ver section-input" key={department.departmentName}>
                <div className="columns is-mobile level-item has-text-centered">
                    <div className="column is-3 vertical-input">                       
                        <span className="">{department.departmentName}</span>
                    </div>
                    <div className="column is-3 vertical-input">
                        <input className="input has-text-centered" type="number" department={department.departmentName} staff="instructor"
                        onChange={(event) => this.setInputValue(event)}/>
                    </div>
                    <div className="column is-3 vertical-input">
                        <input className="input has-text-centered" type="number" department={department.departmentName} staff="student"
                        onChange={(event) => this.setInputValue(event)}/>
                    </div>
                    <div className="column is-1">
                        <span className="">{department.fc}</span>
                    </div>
                    <div className="column is-1">
                        <span className="">{department.ev}</span>
                    </div>
                </div>
            </section>
        ));

        return(  
            
            <div>
                {rowForm}
                <section className="section is-mobile level">
                    <div className="level-left"></div>
                    <div className="level-right">
                        <div className="level-item">
                            <p className="control">
                                <input className="input" type="number" placeholder="FC total" 
                                onChange={(event) => {this.setState({fcTotal: event.currentTarget.value})}}/>
                            </p>
                        </div>
                        <div className="level-item">
                            <p className="level-item"><a className="button cal" 
                            onClick={(event) => this.onCalculateClicked(event)}>Calculate FC</a></p>
                        </div>
                    </div>
                </section>
            </div>            
        )
	  
    }
}